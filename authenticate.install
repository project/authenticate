<?php
/**
 * @file
 * Install, update and uninstall functions for the authenticate module.
 *
 */



/**
 * Implements hook_schema().
*/
function authenticate_schema() {
  $schema['authenticate'] = array(
    'description' => 'Stores primary authentication run records',
    'fields' => array(
      'aid' => array(
        'description' => 'Unique identifier for each authentication run',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'nid' => array(
        'description' => 'NID of the node being authenticated',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'api' => array(
        'description' => 'The search engine API type used to do the authentication',
        'type' => 'varchar',
        'length' => 100,
        'not null' => FALSE,
      ),
      'vid' => array(
        'description' => 'VID of the node being authenticated',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'uid' => array(
        'description' => 'The user who initiated the authentication run',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'started' => array(
        'description' => 'When the run was started',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'completed' => array(
        'description' => 'When the run was completed',
        'type' => 'int',
        'not null' => FALSE,
      ),
      'chunks' => array(
        'description' => 'For "standard" API type runs, the number of chunks the document was split into',
        'type' => 'int',
        'not null' => FALSE,
      ),
      'current_chunk' => array(
        'description' => 'The current chunk being processed',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'current_url' => array(
        'description' => 'The current URL being investigated',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'status' => array(
        'description' => 'Whether or not there have been errors found',
        'type' => 'int',
        'size' => 'tiny',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'matches' => array(
        'description' => 'Count of URL matches found during run',
        'type' => 'int',
        'not null' => FALSE,
      ),
      'errors' => array(
        'description' => 'Error flag that may be set during run',
        'type' => 'int',
        'size' => 'tiny',
        'unsigned' => TRUE,
        'not null' => FALSE,
      ),
      'data' => array(
        'description' => 'Any additional data that may be required. Seriliazed array.',
        'type' => 'text',
        'not null' => FALSE,
      ),
    ),
    'primary key' => array('aid'),
  );

  $schema['authenticate_results'] = array(
    'description' => 'Stores results from URLs which have matched during search run',
    'fields' => array(
      'aid' => array(
        'description' => 'The unique AID from authentication table',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'url' => array(
        'description' => 'matched URL',
        'type' => 'text',
        'not null' => TRUE,
      ),
      'compare' => array(
        'description' => 'the comparison score',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'created' => array(
        'description' => 'when created',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
    ),
  );

  return $schema;
}

/**
 * Implements hook_install().
 */
function authenticate_install() {
  drupal_set_message(t('The tables for the Authenticate module have been created successfully.'));
}

/**
 * Implements hook_uninstall().
*/
function authenticate_uninstall() {
  db_delete('actions')
  ->condition('callback', 'action_auth_%', 'LIKE')
  ->execute();
}

